mod server;
mod session;

use dotenv::dotenv;
use log::warn;
use server::{
    kvantchat_tonic::kvantchat_protocol_server::KvantchatProtocolServer, KvantchatServer,
};
use std::env;
use tonic::transport::Server;

#[tokio::main]
async fn main() -> color_eyre::Result<()> {
    color_eyre::install()?;
    dotenv().ok();
    simple_logger::SimpleLogger::new()
        .with_level(log::LevelFilter::Info)
        .init()?;
    let port = env::var("KVANTCHAT_SERVER_PORT")?;
    warn!("Run server. Port {port}");

    let address = format!("[::1]:{port}").parse()?;
    let config_server = KvantchatServer::default();
    Server::builder()
        .add_service(KvantchatProtocolServer::new(config_server))
        .serve(address)
        .await?;

    Ok(())
}
