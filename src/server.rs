//! Реализация gRPC для KvantChat

pub mod db;

use self::kvantchat_tonic::{RegistrationReplay, RegistrationRequest};
use crate::session::{new_key_session, Sessions};
use kvantchat_tonic::kvantchat_protocol_server::KvantchatProtocol;
use kvantchat_tonic::{LoginReplay, LoginRequest};
use log::{error, info};
use std::sync::Mutex;
use tonic::{Request, Response, Status};

/// Импорт нашего gRPC через tonic
pub mod kvantchat_tonic {
    tonic::include_proto!("kvantchat");
}

/// Дополнительные функции
pub(self) mod detail {
    use super::db::models::User;
    use super::db::solting_password;
    use super::kvantchat_tonic::LoginRequest;
    use super::{new_key_session, Sessions};

    pub(super) fn check_password(request_ref: &LoginRequest, user_info: &User) -> bool {
        solting_password(&request_ref.password, &user_info.solt) == user_info.solted_password
    }

    pub(super) fn session_insert(login: String, sessions: &mut Sessions) -> String {
        let session_key = new_key_session();
        sessions.insert(login, session_key.clone());

        session_key
    }
}

#[derive(Default)]
pub struct KvantchatServer {
    database: Mutex<db::Database>,
    sessions: Mutex<Sessions>,
}

#[tonic::async_trait]
impl KvantchatProtocol for KvantchatServer {
    async fn login(&self, request: Request<LoginRequest>) -> Result<Response<LoginReplay>, Status> {
        let request_ref = request.get_ref();
        let mut database = self.database.lock().unwrap();

        if let Some(user_info) = database.get_user_by_login(&request_ref.login) {
            if detail::check_password(request_ref, &user_info) {
                let mut sessions = self.sessions.lock().unwrap();
                let key_session = detail::session_insert(request_ref.login.clone(), &mut *sessions);

                info!(
                    "Login to account successful! request: {:?}; key_session: {}",
                    request_ref, key_session
                );
                return Ok(Response::new(LoginReplay { key_session }));
            }
        }

        error!("Login to account error! request: {:?}", request_ref);
        Err(Status::permission_denied("wrong login or password"))
    }

    async fn registration(
        &self,
        request: Request<RegistrationRequest>,
    ) -> Result<Response<RegistrationReplay>, Status> {
        let request_ref = request.get_ref();
        let mut database = self.database.lock().unwrap();

        if database.get_user_by_login(&request_ref.login).is_some() {
            return Err(Status::already_exists("account already exists"));
        }

        if let Ok(user_info) = database.insert_user(&request_ref.login, &request_ref.password) {
            let mut sessions = self.sessions.lock().unwrap();
            let key_session = detail::session_insert(request_ref.login.clone(), &mut *sessions);

            return Ok(Response::new(RegistrationReplay {
                key_session,
                login: user_info.login,
                uuid: user_info.id.to_string(),
            }));
        }

        Err(Status::aborted(
            "aborted by database. Please check database",
        ))
    }
}
