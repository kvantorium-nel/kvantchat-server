//! База данных

pub mod models;
pub mod schema;

use self::models::*;
use anyhow::Result;
use diesel::PgConnection;
use diesel::{prelude::*, OptionalExtension};
use schema::users;
use schema::users::dsl::*;

pub struct Database {
    driver: PgConnection,
}

impl Default for Database {
    fn default() -> Self {
        Database::connect(
            &std::env::var("KVANTCHAT_SERVER_DATABASE_URL")
                .expect("Error get KVANTCHAT_SERVER_DATABASE"),
        )
        .expect("Error open database")
    }
}

/// Солённый пароль
pub type SoltedPasswod = String;

/// Посолить пароль
pub fn solting_password(password: &str, solt_for_password: &str) -> SoltedPasswod {
    let password_with_solt = format!("{}:{}", password, solt_for_password);

    use sha1::{Digest, Sha1};
    let mut hasher = Sha1::default();
    hasher.update(password_with_solt);

    format!("{:x}", hasher.finalize())
}

impl Database {
    /// Подключится к нашей базе данных
    pub fn connect(database_url: &str) -> Result<Self> {
        Ok(Self {
            driver: PgConnection::establish(database_url)?,
        })
    }

    /// Добавить пользователя в базу данных
    pub fn insert_user(&mut self, login_user: &str, password: &str) -> Result<User> {
        let solt_for_password = uuid::Uuid::new_v4().to_string();

        let new_solted_password = solting_password(password, &solt_for_password);

        let new_user = NewUser {
            login: login_user,
            solted_password: &new_solted_password,
            solt: &solt_for_password,
        };

        let user: User = diesel::insert_into(users::table)
            .values(&new_user)
            .get_result(&mut self.driver)?;

        Ok(user)
    }

    /// Получить пользователя по логину
    pub fn get_user_by_login(&mut self, login_user: &str) -> Option<User> {
        users
            .filter(login.eq(login_user.to_string()))
            .first(&mut self.driver)
            .optional()
            .expect("error get user by login for database")
    }
}
