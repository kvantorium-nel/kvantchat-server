use super::schema::users;
use diesel::prelude::*;
use uuid::Uuid;

#[derive(Queryable, Clone)]
pub struct User {
    pub id: Uuid,
    pub login: String,
    pub solted_password: String,
    pub solt: String,
}

#[derive(Insertable)]
#[diesel(table_name = users)]
pub struct NewUser<'a> {
    pub login: &'a str,
    pub solted_password: &'a str,
    pub solt: &'a str,
}
