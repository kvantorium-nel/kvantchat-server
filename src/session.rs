//! Управление сессиями

use std::collections::HashMap;

pub type Login = String;

/// Ключ от нашей сессии
///
/// Хранится с помощью uuid_v4
pub type KeySession = String;

/// Здесь хранятся наши сессии
pub type Sessions = HashMap<Login, KeySession>;

pub fn new_key_session() -> KeySession {
    uuid::Uuid::new_v4().to_string()
}
